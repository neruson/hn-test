import axios from 'axios'

const url = 'https://hn.algolia.com/api/v1/'

const search = 'search_by_date?query=nodejs'

const getNodejsStories = async (page: number = 0) => {
  const { data } = await axios.get(`${url}${search}&page=${page}`)
  return data
}

export default {
  getNodejsStories
}