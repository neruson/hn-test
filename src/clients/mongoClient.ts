import { connect } from "mongoose";

const url = process.env.MONGO_DB_URL || 'mongodb://localhost:27017'

const mongoConnect = async () => {
  let client;

  try {
    client = await connect(url);
  } catch(err) {
    throw new Error(`an error has ocurred in mongoose connect: ${String(err)}`);
  }

  return client;
}

export {
  mongoConnect
}