import cron from 'node-cron';
import hnClient from '../clients/hnClient';
import Stories from '../models/Stories';



const gettingStoriesCron = cron.schedule('* * 1 * * *', async () => {
  const data = await hnClient.getNodejsStories();

  if (data.hits && data.hits.length > 0) {
    console.log('---: processing stories :---')
    const storiesData = data.hits.filter((story: any) => {
      const availableToInsert = story.story_id || story.story_title || story.url;
      return availableToInsert;
    }).map((story: any) => {
      return {
        replaceOne: {
          filter: {
            externalId: story.objectID
          },
          replacement: {
            externalId: story.objectID,
            storyId: story.story_id,
            storyTitle: story.story_title,
            storyUrl: story.story_url,
            parentId: story.parent_id,
            createdAt: story.created_at,
            author: story.author,
            comment: story.comment_text,
            addedAt: new Date(),
            tags: story._tags,
          },
          upsert: true
        }
      }
    })
    console.log(`---: stories ${storiesData.length} :---`)
    const result = await Stories.bulkWrite(storiesData)
    console.log(`---: Stories added ${result.insertedCount} :---`)
  }
})

export {
  gettingStoriesCron
}