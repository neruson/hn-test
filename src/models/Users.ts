import { Schema, model, Document, PaginateModel } from 'mongoose';

import { UsersType } from '../types/users.types';
import { encryptPassword } from '../utils/password';

const userSchema = new Schema<UsersType>({
  username: {
    type: String,
    unique: true
  },
  password: {
    type: String
  },
  email: {
    type: String,
    unique: true
  }
});

userSchema.pre('save', function() {
  this.password = encryptPassword(this.password);
});

interface UsersDocument extends Document, UsersType {};

const Users = model<UsersDocument, PaginateModel<UsersDocument>>('Users', userSchema);

export default Users;