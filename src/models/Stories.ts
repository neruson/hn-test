import { Schema, model, Document, PaginateModel } from 'mongoose';
import paginate from 'mongoose-paginate-v2';
import { StoriesType } from '../types/stories.types';

const storiesSchema = new Schema<StoriesType>({
  externalId: {
    type: String,
    unique: true
  },
  storyId: {
    type: Number,
    index: true
  },
  storyTitle: {
    type: String
  },
  storyUrl: {
    type: String
  },
  parentId: {
    type: Number,
    index: true
  },
  createdAt: {
    type: Date,
    index: true
  },
  author: {
    type: String,
    index: true
  },
  comment: {
    type: String
  },
  addedAt: {
    type: Date
  },
  tags: {
    type: [String],
    default: [],
    index: true
  },
  unFollowedList: {
    type: [String],
    default: []
  }
});

storiesSchema.plugin(paginate);

interface StoriesDocument extends Document, StoriesType {};

const Stories = model<StoriesDocument, PaginateModel<StoriesDocument>>('Stories', storiesSchema);

export default Stories;