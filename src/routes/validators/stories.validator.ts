import { Meta, Schema } from "express-validator";
import mongoose from "mongoose";

import Stories from "../../models/Stories";
import { decodeToken } from "../../utils/jwt";

const findStoryById = async (value: string, {req}: Meta) => {
  if (!value) {
    throw new Error('id is required');
  }
  const isMongoId = mongoose.Types.ObjectId.isValid(value);
  const query = isMongoId ? {_id: new mongoose.Types.ObjectId(value)} : {externalId: value};
  const storyExist = await Stories.findOne(query);
  if (!storyExist) throw new Error('story doesn\'t exist');

  req.body.story = storyExist;
  return value;
}

const makeQuery = (data: {[key: string]: any}, authorization: string) => {
  const query: {[key: string]: any} = {
    author: '',
    tags: {$in: []},
    title: {$regex: ''}
  }

  const payload = decodeToken(authorization);

  query.unFollowedList = {$not: {$in: [payload.userId]}}

  if (data.author) query.author = data.author;
  else delete query.author
  if (data.tags && data.tags.length > 0) query.tags.$in = data.tags;
  else delete query.tags
  if (data.title) query.title.$regex = data.title;
  else delete query.title

  return query;
}

const validPage = (params: {[key: string]: any}) => {
  let page = Number(params.page ?? 1);
  if (page <= 0) page = 1;
  return page;
}

const validLimit = (params: {[key: string]: any}) => {
  let limit = Number(params.limit ?? 5);
  if (limit > 5) limit = 5;
  if (limit <= 0) limit = 1;
  return limit;
}

const validSort = (params: {[key: string]: any}) => {
  const defaultValue = 'addedAt';
  let sort = params.sort ?? defaultValue;
  if (!sort.includes('createdAt') || !sort.includes('addedAt')) sort = defaultValue;
  return sort;
}

const findStoriesWithFilter = async (value: string, {req}: Meta) => {
  const authorization = req.headers?.authorization;

  const params = {...req.params, ...req.query};
  const query = makeQuery(params, authorization);
  const page = validPage(params);
  const limit = validLimit(params);
  const sort = validSort(params);

  const stories = await Stories.paginate(query, {sort, page, limit});
  if (stories.docs.length === 0) throw new Error('no stories found');

  req.body.stories = stories;

  return value;
}

const getStoriesSchema: Schema = {
  validator: {
    in: ['params', 'query'],
    custom: {
      options: (value, meta) => findStoriesWithFilter(value, meta)
    }
  }
}

const getOneStorySchema: Schema = {
  id: {
    in: ['params', 'query'],
    custom: {
      options: (value, meta) => findStoryById(value, meta)
    }
  }
}

const unfollowOneStorySchema: Schema = {
  id: {
    in: ['params', 'query'],
    custom: {
      options: (value, meta) => findStoryById(value, meta)
    }
  }
}

export {
  getStoriesSchema,
  getOneStorySchema,
  unfollowOneStorySchema
}