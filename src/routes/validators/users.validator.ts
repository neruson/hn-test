import { Schema } from "express-validator";

import Users from "../../models/Users";
import { validatePassword } from "../../utils/password";

const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/

const registerSchema: Schema = {
  username: {
    in: ['body'],
    custom: {
      options: async (value: string, _) => {
        if (!value) {
          throw new Error('username is required')
        }
        const userExist = await Users.findOne({username: value})
        if (userExist) throw new Error('user exist')
        return value
      }
    }
  },
  email: {
    in: ['body'],
    custom: {
      options: async (value: string, _) => {
        if (!emailRegex.test(value)) {
          throw new Error('email is invalid')
        }
        const userExist = await Users.findOne({email: value})
        if (userExist) throw new Error('user exist')
        return value
      }
    }
  },
  password: {
    in: ['body'],
    custom: {
      options: (value: string, _) => {
        if (value.length < 8) {
          throw new Error('password is too short')
        }
        return value
      }
    }
  }
}

const loginSchema: Schema = {
  email: {
    in: ['body'],
    custom: {
      options: async (value: string, {req}) => {
        if (!emailRegex.test(value)) {
          throw new Error('email is invalid')
        }
        const userExist = await Users.findOne({email: value})
        if (!userExist) throw new Error('user doesn\'t exist')
        req.body.user = userExist;
        return value
      }
    }
  },
  password: {
    in: ['body'],
    custom: {
      options: (value: string, {req}) => {
        if (value !== '' && req.body.user) {
          const passwordValid = validatePassword(value, req.body.user.password);
          if (!passwordValid) throw new Error('password not match');
        } else {
          throw new Error('user or password is wrong')
        }
        return value
      }
    }
  }
}

export {
  registerSchema,
  loginSchema
}
