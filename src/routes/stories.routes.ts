import { Request, Response, Router } from "express";
import { checkSchema } from "express-validator";
import {
  getStories,
  getOneStory,
  unfollowOneStory
} from "../controllers/stories.controllers";
import { getOneStorySchema, getStoriesSchema, unfollowOneStorySchema } from "./validators/stories.validator";

const storiesRoutes = Router();

storiesRoutes.get('/stories', checkSchema(getStoriesSchema), function (req: Request, res: Response) {
  getStories(req, res);
})

storiesRoutes.get('/stories/:id', checkSchema(getOneStorySchema), function (req: Request, res: Response) {
  getOneStory(req, res);
})

storiesRoutes.post('/stories/:id', checkSchema(unfollowOneStorySchema), function (req: Request, res: Response) {
  unfollowOneStory(req, res);
})

export default storiesRoutes;