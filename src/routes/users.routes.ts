import { Request, Response, Router } from "express";
import { checkSchema } from "express-validator";
import { login, register } from "../controllers/users.controllers";
import { loginSchema, registerSchema } from "./validators/users.validator";

const usersRoutes = Router();

usersRoutes.post('/register', checkSchema(registerSchema), function(req: Request, res: Response) {
  register(req, res);
});

usersRoutes.get('/login', checkSchema(loginSchema), function (req: Request, res: Response) {
  login(req, res);
});

export default usersRoutes;