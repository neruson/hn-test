export interface StoriesType {
  externalId: string,
  storyId: number,
  storyTitle: string,
  storyUrl: string,
  parentId: number,
  createdAt: Date,
  author: string,
  comment: string,
  addedAt: Date,
  tags: string[],
  unFollowedList: string[]
}