import express, { Express } from 'express';
import dotenv from 'dotenv';

import { mongoConnect } from "./clients/mongoClient";
import { gettingStoriesCron } from './services/cron';
import usersRoutes from './routes/users.routes';
import storiesRoutes from './routes/stories.routes';
import authorization from './middleware/authorization';

dotenv.config();

const app: Express = express();

mongoConnect()
.then(_ => {
  console.log(`
  ------------------
  MongoDB Connected!
  ------------------
  `)
})
.catch(err => console.error(err));

gettingStoriesCron.start();

// MIDDLEWARES
app.use(express.json());
app.use(authorization);

// ROUTES
app.use(usersRoutes);
app.use(storiesRoutes);

export {
  app
}