import { Request, Response } from "express";
import { validationResult } from "express-validator";
import Users from "../models/Users";
import { encode } from "../utils/jwt";

const register = async (req: Request, res: Response) => {
  const {username, email, password} = req.body;

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const user = await Users.create({username, email, password});

  if (user) {
    return res.status(200).send({message: 'user created!', user: {username: user.username, email: user.email, id: user._id.toString()}})
  }
}

const login = async (req: Request, res: Response) => {
  const { user } = req.body;

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  if (user) {
    const jwt = encode({username: user.username, email: user.email, userId: user._id.toString()})
    return res.status(200).send({jwt})
  }
}

export {
  register,
  login
}