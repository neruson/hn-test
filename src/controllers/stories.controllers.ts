import { Request, Response } from "express";
import { validationResult } from "express-validator";
import { decodeToken } from "../utils/jwt";


const getStories = async (req: Request, res: Response) => {
  const { stories } = req.body;

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  return res.status(200).json(stories)
}

const getOneStory = async (req: Request, res: Response) => {
  const { story } = req.body;

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  return res.status(200).json(story)
}

const unfollowOneStory = async (req: Request, res: Response) => {
  const authorization = req.headers.authorization as string;
  const { story } = req.body;

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { userId } = decodeToken(authorization);

  story.unFollowedList.push(userId);

  await story.save();

  return res.status(200).json({message: "sucess"})
}

export {
  getStories,
  getOneStory,
  unfollowOneStory
}