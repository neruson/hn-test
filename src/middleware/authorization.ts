import Mongoose from 'mongoose';
import { Request, Response, NextFunction } from 'express';

import { decodeToken } from '../utils/jwt';
import Users from '../models/Users';


const authorization = async (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers.authorization;

  if (!req.path.includes('/login') && !req.path.includes('/register')) {
    if (!token) {
      return res.status(403).send({ message: 'You need a token' });
    }
    try {
      const payload = decodeToken(token)
      console.log(payload)
      const userExist = await Users.findOne({_id: new Mongoose.Types.ObjectId(payload.userId)});
  
      if (!userExist) return res.status(403).send({ message: 'User doesn\`t  exist' });
  
      req.params.userId = payload.userId;
  
      req.params.username = payload.username;
  
    } catch (err) {
      console.error(err);
      return res.status(500).send({ message: 'Token invalid' });
    }
  }

  next();
}

export default authorization;