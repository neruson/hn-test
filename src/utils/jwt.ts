import JWT from 'jwt-simple';
import dotenv from 'dotenv';

dotenv.config();

const secret = process.env.SECRET || '';

export function encode(obj: any): string {
  return JWT.encode({...obj}, secret);
}

export function decodeToken(token: string): {[key: string]: any} {
  return JWT.decode(token.replace('Bearer ', ''), secret);
}