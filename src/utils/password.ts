import bcrypt from 'bcryptjs';
import dotenv from 'dotenv';

dotenv.config()

const salt = process.env.SALT || 8

function encryptPassword(password: string): string {
  return bcrypt.hashSync(password, salt);
}

function validatePassword(password: string, hash: string): boolean {
  return bcrypt.compareSync(password, hash);
}

function genSalt(number: number): string {
  return bcrypt.genSaltSync(number);
}

export {
  encryptPassword,
  validatePassword,
  genSalt
}