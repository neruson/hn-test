import "jest-expect-jwt";
import { encode, decodeToken } from '../../src/utils/jwt';

describe("Test jwt utils", () => {
  test("It should encode an object", () => {
    const object = {
      hello: "world"
    }

    const encodeObj = encode(object);

    expect(encodeObj).toBeTokenMatching({ hello: "world"})
  })

  test("It should decode an token", () => {
    const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvcmxkIn0.tgpkl5cBQw7TISyignP491ZYBX8Cb7O7rydEzn7jqGc"

    const decodeObj = decodeToken(token);

    expect(decodeObj).toStrictEqual({hello: "world"})
  })
})