import { encryptPassword, validatePassword } from '../../src/utils/password';

describe("Test password utils", () => {
  test("It should encrypt an password", () => {
    const password = 'SuperP4ssword'

    const encrypt = encryptPassword(password);

    expect(typeof encrypt).toBe("string")
  })

  test("It should validate that password is the same", () => {
    const password = 'SuperP4ssword'

    const encrypt = encryptPassword(password);

    const isTheSame = validatePassword(password, encrypt);

    expect(isTheSame).toBe(true)
  })
})