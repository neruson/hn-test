import dotenv from 'dotenv';
dotenv.config({path: '../../.env.test'})

import request from 'supertest';
import { app } from '../../src/app';

describe('Test stories routes', () => {
  let login: any;

  let storyId: string;

  beforeEach(async () => {
    login = await request(app)
      .get('/login')
      .send({
        "email": "testEmail@test.com",
        "password": "testPassword"
      })
  })

  test('It should fail because we dont have jwt', async () => {
    const response = await request(app).get('/stories');

    console.log(response.body)
    expect(response.body.message).toBeDefined()
    expect(response.body.message).toBe('You need a token')
  })

  test('It should list 5 stories', async () => {
    const response = await request(app).get('/stories').set('Authorization', login.body.jwt);

    expect(response.body.docs).toBeDefined()
    expect(response.body.docs.length).toBe(5)

    storyId = response.body.docs[2]._id.toString()
  })

  test('It Should get one story', async () => {
    const response = await request(app).get(`/stories/${storyId}`).set('Authorization', login.body.jwt);

    console.log(response.body)

    expect(response.body).toBeDefined()
    expect(response.body._id).toBe(storyId)
  })

  test('It should unfollow one story', async () => {
    const listStories = await request(app).get('/stories').set('Authorization', login.body.jwt);
    const storyToUnfollowId = listStories.body.docs[2]._id;

    expect(listStories.body.docs[2]._id).toBe(storyToUnfollowId);

    const unfollowStory = await request(app).post(`/stories/${storyToUnfollowId}`).set('Authorization', login.body.jwt);

    expect(unfollowStory.body.message).toBe("sucess");

    const listStoriesAgain = await request(app).get('/stories').set('Authorization', login.body.jwt);

    expect(listStoriesAgain.body.docs[2]._id).not.toBe(storyToUnfollowId);
  })
})