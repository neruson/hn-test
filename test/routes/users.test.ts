import dotenv from 'dotenv';
dotenv.config({path: '../../.env.test'})

import mongoose from 'mongoose';
import request from 'supertest';
import { app } from '../../src/app';
import Users from '../../src/models/Users';

describe('Test user routes', () => {
  let userCreatedId: string;

  afterAll(async () => {
    await Users.deleteOne({_id: new mongoose.Types.ObjectId(userCreatedId)});
  })

  test('It should register an user', async () => {
    const response = await request(app)
      .post('/register')
      .send({
        "username": "test1",
        "email": "test1@testing.com",
        "password": "passSuper.27"
      });

    console.log(response.body)
    expect(response.body.message).toBeDefined()
    expect(response.body.message).toBe('user created!')
    expect(response.body.user.id).toBeDefined()

    userCreatedId = response.body.user.id;
  })

  test('It should login and get jwt', async () => {
    const response = await request(app)
      .get('/login')
      .send({
        "email": "test1@testing.com",
        "password": "passSuper.27"
      })

    expect(response.body.jwt).toBeDefined()
    expect(response.body.jwt).toBeTokenMatching({
      username: 'test1',
      email: 'test1@testing.com',
      userId: userCreatedId
    })
  })
})