import { app } from './src/app';
import dotenv from 'dotenv';

dotenv.config();

const port: string = process.env.PORT || '3000';

// LISTEN
app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});