import hnClient from '../src/clients/hnClient';
import { mongoConnect } from '../src/clients/mongoClient';
import Stories from '../src/models/Stories';
import Users from '../src/models/Users';

console.log("----: populator inited :----");

async function init() {
  const client = await mongoConnect()
    .catch(err => console.error(err));

  if (client) {
    await Users.deleteOne({username: 'testingUser'});

    for (const index of [...Array(50).keys()]) {
      console.log(`----: getting page ${index} of stories :----`);
      const data = await hnClient.getNodejsStories(index);

      if (data.hits && data.hits.length > 0) {
        console.log('processing stories');
        const storiesData = data.hits.filter((story: any) => {
          const availableToInsert = story.story_id || story.story_title || story.url;
          return availableToInsert;
        }).map((story: any) => {
          return {
            replaceOne: {
              filter: {
                externalId: story.objectID
              },
              replacement: {
                externalId: story.objectID,
                storyId: story.story_id,
                storyTitle: story.story_title,
                storyUrl: story.story_url,
                parentId: story.parent_id,
                createdAt: story.created_at,
                author: story.author,
                comment: story.comment_text,
                addedAt: new Date(),
                tags: story._tags,
              },
              upsert: true
            }
          }
        });
        console.log(`stories ${storiesData.length}`);
        await Stories.bulkWrite(storiesData);
        console.log(`Finish page ${index}`);
      }
    }

    await Users.create({
      username: 'testingUser',
      password: 'testPassword',
      email: 'testEmail@test.com'
    });

    const totalStories = await Stories.countDocuments();

    console.log(`${totalStories} stories populate in db`);

    client.disconnect()
  } else {
    process.exit()
  }
}

init();

