module.exports = {
  preset: 'ts-jest',
  setupFilesAfterEnv: ["jest-expect-jwt"],
};